import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  inputValue: any;

  filterArr1: string[] = [];
  filterArr2: string[] = [];
  filterArr3: string[] = [];
  filterArr4: string[] = [];

  filter1: any;
  filter2: any;

  filter3: any;
  filter4: any;

  savebutton = false;
  saveAdit = false;



  addTags(form: any) {
    console.log(form.inputValue);
    if (this.saveAdit === false) {
      this.filter1 = form.inputValue.split(',');
    } else {
      this.filter1 = form.split(',');
      this.filterArr1 = [];
      this.filterArr2 = [];
      this.filterArr3 = [];
    }
    for (let i = 0; i < this.filter1.length; i++) {
      this.filter2 = this.filter1[i].split(';');
      for (let j = 0; j < this.filter2.length; j++) {
        this.filterArr1.push(this.filter2[j]);
      }
    }

    for (let a = 0; a < this.filterArr1.length; a++) {
      this.filter3 = this.filterArr1[a].split(' ');
      for (let b = 0; b < this.filter3.length; b++) {
        this.filterArr2.push(this.filter3[b]);
      }
    }

    for (let c = 0; c < this.filterArr2.length; c++) {
      this.filter4 = this.filterArr2[c].split('\n');
      for (let d = 0; d < this.filter4.length; d++) {
        const toNumber = this.filter4.map(Number);
        if (toNumber[d] > 0 || toNumber[d] < 0) {
          this.filterArr3.push(toNumber[d]);
        }
      }
    }
    this.inputValue = '';
    if (this.saveAdit === false) {
      localStorage.setItem('dataSource', JSON.stringify(this.filterArr3));
    } else {
      localStorage.setItem('dataSource', JSON.stringify(this.filterArr3));
      this.saveAdit = false;

    }
    this.filterArr4 = JSON.parse(localStorage.getItem('dataSource'));
  }

  delete(data): void {
    this.filterArr4.splice(this.filterArr4.indexOf(data), 1);
    localStorage.setItem('dataSource', JSON.stringify(this.filterArr4));
  }

  edit() {
    this.inputValue = this.filterArr4;
    this.savebutton = !this.savebutton;
  }

  saveTags(value: any) {
    this.saveAdit = true;
    if (Array.isArray(this.inputValue)) {
      console.log('yes');
      this.addTags(this.inputValue.join());
    } else {
      console.log('not');
      this.addTags(this.inputValue);
    }
  }

  ngOnInit(): void {
    this.filterArr4 = JSON.parse(localStorage.getItem('dataSource'));
  }

}
